
# Praveen Kumar

praveenkajla8099@gmail.com

3 years experience in building full stack products, end users  from 10 - 1M. 

## Employment History

**Full Stack Developer at Loco [ part of Pocket Aces ], Bangalore**
June 2018 — Present

> :Collapse  label=**LIU ( Loco Streaming Platform )**
>
> - worked on the whole streaming infra and backend
> - Backend [ nginx + ffmpeg + rtmp + lua, Redis, SQS ],
> Infra [ K8s + envoy  + AWS]

> :Collapse  label=**_Ivory ( Streaming APIS )_**
>
> Backend [ hapi.js, _DynamoDB_ , Redis, SQS ],
> Infra [ K8s + AWS]

> :Collapse  label=**_League ( predicting league/games outcomes )_**
>
> Backend [ hapi.js, _DynamoDB_ , Redis, SQS ], 
> Dashboard [ ReactJs + Redux, Serverless, AWS-Cognito ]

> :Collapse  label=**_LOCO-HOST ( Prompter for the live game host )_**
>
> Frontend - [ ReactJs + Redux, WS ]

> :Collapse  label=ZAP 
>
>( Admin dashboard for contests, leagues & notifications)
> Frontend [ ReactJs + Redux ]
> Backend [ Django ( contests ), hapi.js ( league ) ]

**Full Stack Engineer at Pocket Aces, Mumbai**
November 2017 — June 2018
> :Collapse  label=**_LIPI_**
>
> add subs to videos [ Node.js, FFmpeg ], Frontend [ ReactJs + Redux ]

> :Collapse  label=Kliento
>
> FB, YT views & other metrics  [ ReactJs + Redux ],
> Backend [ Node.js ]

**Software Intern at EMIAC Web Solutions, Jaipur**
 May 2016 — July 2016

<br/>

**Education**
B.Tech ( CSE ), Malaviya National Institute Of Technology , Jaipur
2017


**Details**


**Skills**
JavaScript
Node.js
K8s
DynamoDB
Python
Redis
Typescript
React + Redux
HTML/CSS
Git


**Hobbies**
Sports, Indie cinema,
Travelling,
FIFA

**Languages**
Hindi
English





> :Buttons
> > :Button label=LinkedIn, url=https://www.linkedin.com/in/praveen-kumar-k/
>
> > :Button  label=Github, url=https://github.com/praveenkumarKajla/


> :ToCPrevNext