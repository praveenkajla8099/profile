# 4. Median of Two Sorted Arrays


Given two sorted arrays **nums1** and **nums2** of size **m** and **n** respectively, return the median of the two sorted arrays.


Example 1:

>
> Input: nums1 = [1,3], nums2 = [2]
>
> Output: 2.00000
>
> Explanation: merged array = [1,2,3] and median is 2.

# Approach 
First approach is quite straight forward , merge the two sorted array & find the median

> if odd, median = middle element
>
> if even, median = average of two middle elements

Another approach would be to divide the two arrays in two parts left & right such that the 

> combined length of left section === combined length of right section
>
> max(left_part)≤min(right_part)
>
> median  = ( max(left_part)+min(right_part) ) / 2

now to find such positions *i* & *j* for array *A* & *B*  of length m & n we have to keep some conditions in mind

> 1. m <=n 
>
> 2. j = (m+n+1/2) - i 
>
> cuz => 2(i+j) = m+n => left + right ==> merged array

To find such index in lesser time we can use binary search here where our condition would be 

> the window length for binary search initally is iMin = 0 & iMax = m
>
> we start from i = (iMin + iMax)/2 -- middle of smaller array 
>
> then j = (m+n+1)/2 - i 
>
> partion A at i & B at j
>
*& every time we see that the largest element in the left (which is at i-1) of this partion is greater than smallest element in the partition of B i.e. at j*
>
> A[i-1] > B[j] then  iMax = i - 1; // i is too big
>
> & if B[j-1] > A[i] then  iMin = i + 1; // i is too small

> else we have found our perfect partition 


```javascript


/**
 * @param {number[]} nums1
 * @param {number[]} nums2
 * @return {number}
 */
var findMedianSortedArrays = function(nums1, nums2) {
    let m = nums1.length
    let n = nums2.length
    if( m > n){
        let temp = nums1
        nums1 = nums2
        nums2 = temp
        let tmp = m
        m = n
        n = tmp
    }
    let iMin = 0, iMax = m, halfLen = Math.floor((m+n+1)/2);
    while(iMin <= iMax){
        
        let  i = Math.floor((iMin+iMax)/2)
        let j = halfLen - i
        
        
        if(i < iMax && nums2[j-1] > nums1[i]){
            iMin = i + 1
        }
        
        else if(i > iMin && nums1[i-1] > nums2[j]){
            iMax = i - 1
        }
       
        
        else{
            let maxLeft = 0
            if(i === 0) maxLeft = nums2[j-1]
            else if(j === 0) maxLeft = nums1[i-1]
            else maxLeft = Math.max(nums2[j-1],nums1[i-1])
            if((m+n) % 2 === 1){
                return maxLeft
            }
            let minRight = 0
            if(i === m) minRight = nums2[j]
            else if(j === n) minRight = nums1[i]
            else minRight = Math.min(nums2[j],nums1[i])
            return ((minRight+maxLeft)/2)
        }
    }
    return 0
    
};

```

> :ToCPrevNext


