# Leetcode 1253. Reconstruct a 2-Row Binary Matrix

Given the following details of a matrix with n columns and 2 rows :

- The matrix is a binary matrix, which means each element in the matrix can be 0 or 1.
- The sum of elements of the 0-th(upper) row is given as upper.
- The sum of elements of the 1-st(lower) row is given as lower.
- The sum of elements in the i-th column(0-indexed) is colsum[i], where colsum is given as an integer array with length n.

Your task is to reconstruct the matrix with upper, lower and colsum.

Return it as a 2-D integer array.

If there are more than one valid solution, any of them will be accepted.

If no valid solution exists, return an empty 2-D array.

# Approach & Solution

first let's think of all the edge case for wrong input

- sum of upper row + sum of lower row  should be == sum of all the column sums
- number of columns with sum == 2 should be less than **min(upper,lower)**

```javascript

    let totalSum = 0
    let numTwo = 0
    for(let i=0;i<colsum.length;i++){
       totalSum += colsum[i]
        if(colsum[i] == 2){
            numTwo +=1
        }
    }
    if(totalSum !== upper+lower || numTwo> Math.min(upper,lower))return []

```

Now the problem is all about finding the value at each row & column
> if *colsum[i] === 0*  or *colsum[i] === 2* only 0 or 1 respectively is possible at *result[row][column]*
>
> in case of *colsum[i] === 1*, 0 & 1 are possible but we will also have to check if the remaining sum of upper or lower row is > 0


```javascript
    const result = []
    const upperList = []
    const lowerList = []
    
    for(let i=0;i<colsum.length;i++){
        if(colsum[i] === 0){
            upperList.push(0)
            lowerList.push(0)
        }
        if(colsum[i] === 2){
            upperList.push(1)
            lowerList.push(1)
            upper -=1
            lower -=1
        }
        if(colsum[i] === 1){
            if(upper > lower){
                 upperList.push(1)
                lowerList.push(0)
                upper-=1
            }else{
                 upperList.push(0)
                lowerList.push(1)
                lower-=1
            }
        }
        
    }
    result.push(upperList)
    result.push(lowerList)
    return  result




```
> :ToCPrevNext
