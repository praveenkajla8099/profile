# Leetcode | 169. Majority Element

Given an array nums of size n, return the *majority element.*

The majority element is the element that appears more than *⌊n / 2⌋* times. You may assume that the majority element always exists in the array.



Example 2:

>
> Input: nums = [2,2,1,1,1,2,2]
>
> Output: 2
>

# Approach

Only one number (if it exists) can be candidate for this condition of appearing more than *Math.floor(n / 2)* time


let count represents that numbers count, which we will initialize to 0

as soon as we see count == 0 we set candidate to nums[i]

if nums[i] === candidate count +=1 else count -=1

```javascript
/**
 * @param {number[]} nums
 * @return {number}
 */
var majorityElement = function(nums) {
    
    let count = 0
    
    let candidate = 0
    
    for(let i = 0;i<nums.length;i++){
        if(count === 0){
            candidate = nums[i]
        }
        count += candidate === nums[i] ? 1 : -1
    }
    return candidate
    
};


```