# Leetcode 299. Bulls and Cows

You are playing the Bulls and Cows game with your friend.

You write down a secret number and ask your friend to guess what the number is. When your friend makes a guess, you provide a hint with the following info:

The number of "bulls", which are digits in the guess that are in the correct position.
The number of "cows", which are digits in the guess that are in your secret number but are located in the wrong position. Specifically, the non-bull digits in the guess that could be rearranged such that they become bulls.
Given the secret number *secret* and your friend's guess *guess*, return the hint for your friend's guess.

The hint should be formatted as **"xAyB"**, where x is the number of bulls and y is the number of cows. Note that both secret and guess may contain **duplicate digits.**


# Approach 

lets create two variables bulls & cows to handle the total number of digits in correct position & digits which are in secret but at wrong position.

counting bulls is easy, as soon as we see number in secret & guess at same position we increment.

```javascript

    let bulls = 0
    let cows = 0
    for(let i = 0;i<secret.length;i+=1){
        if(secret[i] === guess[i]){
            bulls+=1
        }
    }

```

to figure out cows we first store the frequency of every other number which is not at correct position

so our above code looks something like this

```javascript

    let bulls = 0
    let cows = 0
    let frequency = {}
    for(let i = 0;i<secret.length;i+=1){
        if(secret[i] === guess[i]){
            bulls+=1
        }else{
            frequency[secret[i]] = frequency[secret[i]] 
                ? frequency[secret[i]]+1 
                : 1
        }
    }

```

So what do we do with this frequency map

Simple, we go though the guess string now & whenever we see a number which is not in secret at same position we check if that numbers frequency is > 0 in frequency map

we increment the cows counter & reduce the frequency by 1


# Solution

```javascript

/**
 * @param {string} secret
 * @param {string} guess
 * @return {string}
 */
var getHint = function(secret, guess) {
    
    let bulls = 0
    let cows = 0
    let encountered = {}
    for(let i = 0;i<secret.length;i+=1){
        if(secret[i] === guess[i]){
            bulls+=1
        }else{
            encountered[secret[i]] = encountered[secret[i]] 
                ? encountered[secret[i]]+1 
                : 1
        }

    }
    for(let i = 0;i<guess.length;i+=1){
        
        if(secret[i] !== guess[i]){
            if(encountered[guess[i]] && encountered[guess[i]] > 0){
                cows +=1
                encountered[guess[i]] -=1
            }
        }
        
        
    }
    return `${bulls}A${cows}B`
    
};

```

> :ToCPrevNext
