# Leetcode | 80. Remove Duplicates from Sorted Array II

Given a sorted array nums, remove the duplicates in-place such that duplicates appeared at most twice and return the new length.
Do not allocate extra space for another array; you must do this by modifying the input array in-place with O(1) extra memory.

### Clarification:

Confused why the returned value is an integer, but your answer is an array?
Note that the input array is passed in by reference, which means a modification to the input array will be known to the caller.

Internally you can think of this:
```
// nums is passed in by reference. (i.e., without making a copy)
int len = removeDuplicates(nums);

// any modification to nums in your function would be known by the caller.
// using the length returned by your function, it prints the first len elements.
for (int i = 0; i < len; i++) {
    print(nums[i]);
}
```


<br>

<hr>

## Approach

To keep duplicates max upto 2 & replacing third value onwards with the next unique value

for example if we have something like this
> [1,2,2,2,2,3,3,3] 
> the value at index 3 i.e. 2 should be replaced with next unique value i.e. 3 at index 5

So we keep two pointers ptr1 & ptr2 

ptr2 just iterates over the list & ptr1 is updated to next position whenever we see value such that

```
nums[ptr1-2] != nums[ptr2] --> ptr1-2 cuz we want max 2 duplicates  

```


<br>
Then we just delete the rest of the numbers to return new length

```js | index.js
    nums.length = slowPtr // @see https://stackoverflow.com/questions/4804235/difference-between-array-length-0-and-array
    return slowPtr
```



# solution
```js
/**
 * @param {number[]} nums
 * @return {number}
 */
var removeDuplicates = function(nums) {
    

    
    const length = nums.length
    if(length < 3) return length
    
    //     so we will solve by keeping two pointer
    let slowPtr = 2
    for(let fastPtr = 2;fastPtr<length;fastPtr++){
        if(nums[slowPtr-2] !== nums[fastPtr] ){
            nums[slowPtr] = nums[fastPtr]
            slowPtr+=1
        }
    }
    nums.length = slowPtr
    return slowPtr
    
    
};
```



> :ToCPrevNext