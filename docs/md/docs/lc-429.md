# Leetcode 429. N-ary Tree Level Order Traversal

Given an n-ary tree, return the level order traversal of its nodes' values.

*Nary-Tree input serialization is represented in their level order traversal, each group of children is separated by the null value (See examples).*


Example 1:

![ex1](https://assets.leetcode.com/uploads/2018/10/12/narytreeexample.png)

>
>Input: root = [1,null,3,2,4,null,5,6]
>
>Output: [[1],[3,2,4],[5,6]]




Example 2:

![ex2](https://assets.leetcode.com/uploads/2019/11/08/sample_4_964.png)

>
> Input: root = [1,null,2,3,4,5,null,null,6,7,null,8,null,9,10,null,null,11,null,12,null,13,null,null,14]
>
> Output: [[1],[2,3,4,5],[6,7,8,9,10],[11,12,13],[14]]



# Approach

This one is straightforward BFS problem (though we can use DFS too)

we hold all the nodes at a certain level in a queue and push each node's children in the queue for further processing


```javascript

/**
 * // Definition for a Node.
 * function Node(val,children) {
 *    this.val = val;
 *    this.children = children;
 * };
 */

/**
 * @param {Node|null} root
 * @return {number[][]}
 */
var levelOrder = function(root) {
    if(!root)return []
    let queue = [root]
    const levelOrder = []
    while(queue.length != 0){
        const next = []
        let size = queue.length
        while(size>0){
            const node = queue.shift()
            if(node){
                if(node.children)queue = [...queue, ...node.children]
                next.push(node.val)
            }
            size-=1
        }
        levelOrder.push(next)
    }
    return levelOrder
};


```

> :ToCPrevNext